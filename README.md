[![](https://discordapp.com/api/guilds/96753964485181440/widget.png?style=banner2)](https://discord.gg/ngNQjT5) [![](https://s12.directupload.net/images/200916/joj33k55.png)](https://twitter.com/kreezxil) [![](https://s12.directupload.net/images/200916/efhmdjhg.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fwww.reddit.com%252fr%252fMinecraftModdedForge)

[![Bisect Hosting](https://www.bisecthosting.com/images/logos/dark_text@1538x500.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil)

If you want a server setup for the Any mod pack with zero effort, get a [server with BisectHosting](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fbisecthosting.com%252fkreezxil) and receive 25% off your first month as a new customer using the code kreezxil

## Videos

**NOTE: Version 2.0.2 and on are for version 1.8.9 of Forge**

## All the blocks:

Spoiler (click to show)

![All the blocks](http://i.imgur.com/asfNSoX.png)

These are all of the compressed blocks added by my mod.

## Description

For those times when your inventories are getting full of the common blocks that you just can't seem to part with long enough, or dump into the void, a trashcan, or an active volcano, now you can compress and keep them!

The blocks become harder to harvest and require stronger tools as you go up the tiers and they also becomes more blast resistant. I've left a lot of room for expansion in case some of you guys and gals want to craft blocks of a compression level far exceeding the 8th tier.

Check out the compressed coal blocks, taking them beyond the 4th tier will give you a stack of diamonds, and each tier is 10x the fuel level of the previous tier. Tier 0 is the coal block and it has a burn time of 800.

Caveat: You should notice a recurring pattern on all of the blocks, I've used a simple 4 bit binary system to do the art. Meaning you can encode messages on your walls with these blocks, or just look at them. However, if necessary, the current system can be expanded to 16 levels of compression. I would like to find a procedural way to do that personally as that would allow me to compress anything regardless if I have access to its texture or not.

You can find the cutting edge description and images of what I am doing on my [Imgur Album for Kreezxil's Compressed Blocks](https://www.curseforge.com/linkout?remoteUrl=http%253a%252f%252fimgur.com%252fa%252fK18jm) before I actually get around to updating the texts and images here (but only by a few minutes to a couple of hours at best). However, the files are current here at Curseforge.

To begin creating any of the compressed chains, all you have to do is combine 9 of the base item or block in a crafting grid as indicated by the compressed block chain to achieve the first tier of that chain, then 9 of that tier for next tier and so on and so forth. Example, 9 cobblestone will get you one compressed cobblestone, just like 9 ender pearls will get you one compressed ender pearl.

**This mod also works on a server for both 1.8 and 1.8.8.**

## **Shout Outs:**

*   Much appreciation is given to The\_Fireplace for showing me an exceptionally optimal way to recode the mod.
*   jeffryfisher on the Minecraftforge forums for pointing out some oopsies and expanding my mind also known as [Uncle Jeff](http://minecraft.curseforge.com/search?search=Uncle Jeff)here on CurseForge.
*   desieben07 on Minecraftforge forums for being brutally honest at all times.
*   Lex Manos of Minecraftforge without whom modding Minecraft would be a complete pain in the rear.

And of course, all the time, all Glory to the Most High!

## **ROADMAP:**

Spoiler (click to hide)

TODO:

balancing (ongoing for now),

A black hole to be crafted from several of the top tier compressed blocks, each tier being successively stronger than the last. The black hole will do what you think it will do, and will probably either require a special tool or event to remove from your world once placed. I'm contemplating allowing it to form a white hole in the world or one of your servers dimensions to spew forth things that it sucks up. Possibly even adding quasar effects and what not. Really not sure where to go with it, of course everything will be one step at a time and everything is subject to change. One thing is for sure, you don't want to be near the black hole when you place it. By the time I make this, we'll be using configs, currently not implemented, so that it can be turned off or tuned as the case may be.

I decided against clay blocks except for the default vanilla one.

Rework all of the image patterns. I'm thinking of a new one that is universal, similar to what I did for the quick and dirty update that I just did. If you want to do the community proud and have the time on your hands to redo the textures, then head on over the Source and get the block textures and redesign as many of them as you care to, send them to me and I'll get them added as an option.

I've been thinking about the compressed redstone block quite a bit lately. I think that for each tier of compression it should be able to emit a redstone signal the default length x it's tier + the default amount. So that a block of redstone will push a signal 16 block, then compressed would send that signal 32 blocks (16 \* 1 + 16), etc. This could feasibly allow some advanced parallel redstone compression in your loaded chunks and in the case of chunk loaded blocks, an extreme long range explosion, or what have you.

Should you want to redo all of the textures, you can, and feel free to message me a link to an updated image archive so that I can import them into the mod. You will get full credit for your graphics abilities if I use your work.

## **Backporting:****Why it's not gonna Happen**

Spoiler (click to hide)

*   [Ultra Compression Mod](http://www.curse.com/mc-mods/minecraft/222400-ultra-compression-mod)(1.7.10)
*   [Condensed Block Mod Reborn](http://www.curse.com/mc-mods/minecraft/225226-condensed-block-mod-reborn)(1.7.10)
*   [Extra Utilities](http://www.curse.com/mc-mods/minecraft/225561-extra-utilities)(1.7.10)

**Compressed Cobblestone**

![](http://i.imgur.com/E1fLPEO.png)

Spoiler (click to hide)

TIER	     HARDNESS	RESISTANCE
Compressed	3	 0.525
Double	       12	 2.1
Triple	       27	 4.725
Quadruple      48	 8.4
Quintuple      75	13.125
Sextuple      108	18.9
Septuple      147	25.725
Octuple	      192	33.6

Harvest Levels: WOOD, WOOD, STONE, STONE, IRON, IRON,
			DIAMOND, DIAMOND

**Compressed Dirt**

**![](http://i.imgur.com/9MssCVM.png)**

Spoiler (click to hide)

TIER	      HARDNESS	RESISTANCE
Compressed	 0.4	 0.07
Double  	 1.6	 0.28
Triple  	 3.6	 0.63
Quadruple	 6.4	 1.12
Quintuple	10	 1.75
Sextuple	14.4	 2.52
Septuple	19.6	 3.43
Octuple 	25.6	 4.48

Harvest Levels: WOOD, WOOD, WOOD, WOOD, STONE, STONE,
		STONE, STONE

**Compressed Coal**

**![](http://i.imgur.com/PIG8tpG.png)**

Better storage, better fuel and DIAMONDS!!!

Spoiler (click to hide)

TIER	      HARDNESS	RESISTANCE
Compressed	0.6	 0.105
Double  	2.4	 0.42
Triple  	5.4	 0.945
Quadruple	9.6	 1.68

Harvest Levels: STONE, IRON, IRON, IRON

**Compressed Flint**

![](http://i.imgur.com/9VCb4gO.png)

Spoiler (click to hide)

TIER	      HARDNESS	RESISTANCE
Compressed	1.5	  0.2625
Double  	6	  1.05
Triple  	13.5	  2.3625
Quadruple	24	  4.2
Quintuple	37.5	  6.5625
Sextuple	54	  9.45
Septuple	73.5	 12.8625
Octuple 	96	 16.8

Harvest Levels: STONE, STONE, STONE, IRON, IRON, IRON,
			DIAMOND, DIAMOND

**Compressed Gravel**

![](http://i.imgur.com/aTuP8G2.png)

All of these blocks have a 10% chance to return Compressed Flint of a tier one lower.

Spoiler (click to hide)

TIER	      HARDNESS	RESISTANCE
Compressed	  7	  14
Double  	 28	  56
Triple  	 63	 126
Quadruple	112	 224
Quintuple	175	 350
Sextuple	252	 504
Septuple	343	 686
Octuple 	448	 896

Harvest Levels: STONE, STONE, STONE, STONE, IRON,
			IRON, IRON, IRON

**Compressed Sand**
![](http://i.imgur.com/H035vmL.png)

Spoiler (click to hide)

TIER	       HARDNESS	 RESISTANCE
Compressed	  4.28	   0.749
Double  	 17.12	   2.996
Triple  	 38.52	   6.741
Quadruple	 68.48	  11.984
Quintuple	107	  18.725
Sextuple	154.08	  26.964
Septuple	209.72	  36.701
Octuple 	273.92	  47.936

Harvest Levels: WOOD, WOOD, WOOD, WOOD, STONE,
			STONE, STONE, STONE

**Compressed Red Sand**
![](http://i.imgur.com/Eri7ha7.png)

Spoiler (click to hide)

TIER	       HARDNESS	 RESISTANCE
Compressed	  4.28	   0.749
Double  	 17.12	   2.996
Triple  	 38.52	   6.741
Quadruple	 68.48	  11.984
Quintuple	107	  18.725
Sextuple	154.08	  26.964
Septuple	209.72	  36.701
Octuple 	273.92	  47.936

Harvest Levels: WOOD, WOOD, WOOD, WOOD, STONE,
			STONE, STONE, STONE

**Compressed Clay**

**![](http://i.imgur.com/D9V4YoG.png)**

Spoiler (click to hide)

TIER	      HARDNESS	RESISTANCE
Compressed	  7	   14
Double  	 28	   56
Triple  	 63	  126
Quadruple	112	  224
Quintuple	175	  350
Sextuple	252	  504
Septuple	343	  686
Octuple 	448	  896

Harvest Levels: WOOD, WOOD, WOOD, STONE, STONE, STONE,
			IRON, IRON

**Compressed Diamond**

![](http://i.imgur.com/gq9ePwM.png)

Spoiler (click to hide)

TIER        HARDNESS    RESISTANCE
Compressed     8           2.2
Double        32           8.8
Triple        72          19.8
Quadruple    128          35.2
Quintuple    200          55
Sextuple     288          79.2
Septuple     392         107.8
Octuple      512         140.8

Harvest Levels: DIAMOND, DIAMOND, DIAMOND, DIAMOND,
                DIAMOND, DIAMOND, DIAMOND, DIAMOND

**Compressed Emerald**

![](http://i.imgur.com/WO54iET.png)

Spoiler (click to hide)

TIER        HARDNESS    RESISTANCE
Compressed     8           2.2

## Powered by

## [![](https://i.imgur.com/LVfqh8h.png)](https://www.curseforge.com/linkout?remoteUrl=https%253a%252f%252fgdevs.io%252f)

Double        32           8.8
Triple        72          19.8
Quadruple    128          35.2
Quintuple    200          55
Sextuple     288          79.2
Septuple     392         107.8
Octuple      512         140.8

Harvest Levels: DIAMOND, DIAMOND, DIAMOND, DIAMOND,
                DIAMOND, DIAMOND, DIAMOND, DIAMOND

**Compressed Redstone**

![](http://i.imgur.com/l19umSh.png)

Spoiler (click to hide)

TIER	      HARDNESS	RESISTANCE
Compressed	  7	  14
Double  	 28	  56
Triple  	 63	 126
Quadruple	112	 224
Quintuple	175	 350
Sextuple	252	 504
Septuple	343	 686
Octuple 	448	 896

Harvest Levels: STONE, STONE, STONE, STONE, IRON,
			IRON, IRON, IRON

**Compressed Obsidian**

![](http://i.imgur.com/Zv35olS.png)

Spoiler (click to hide)

TIER        HARDNESS    RESISTANCE
Compressed     8           2.2
Double        32           8.8
Triple        72          19.8
Quadruple    128          35.2
Quintuple    200          55
Sextuple     288          79.2
Septuple     392         107.8
Octuple      512         140.8

Harvest Levels: DIAMOND, DIAMOND, DIAMOND, DIAMOND,
                DIAMOND, DIAMOND, DIAMOND, DIAMOND

**Compressed Netherrack**

![](http://i.imgur.com/uyCUnv0.png)

Spoiler (click to hide)

TIER	      HARDNESS	RESISTANCE
Compressed	  7	  14
Double  	 28	  56
Triple  	 63	 126
Quadruple	112	 224
Quintuple	175	 350
Sextuple	252	 504
Septuple	343	 686
Octuple 	448	 896

Harvest Levels: STONE, STONE, STONE, STONE, IRON,
			IRON, IRON, IRON

**Compressed Gold**

![](http://i.imgur.com/NAPT648.png)

Spoiler (click to hide)

TIER	       HARDNESS 	RESISTANCE
Compressed	 1.53	          0.057834
Double	         6.12             0.231336
Triple	        13.77	          0.520506
Quadruple       24.48         	  0.925344
Quintuple  	38.25        	  1.44585
Sextuple   	55.08        	  2.082024
Septuple   	74.97	          2.833866
Octuple     	97.92	          3.701376

Harvest Levels: STONE, STONE, STONE, IRON, IRON, IRON,
		DIAMOND, DIAMOND

**Compressed Lapis**

![](http://i.imgur.com/qltiepm.png)

Spoiler (click to hide)

TIER	       HARDNESS 	RESISTANCE
Compressed	 1.53	          0.057834
Double	         6.12             0.231336
Triple	        13.77	          0.520506
Quadruple       24.48         	  0.925344
Quintuple  	38.25        	  1.44585
Sextuple   	55.08        	  2.082024
Septuple   	74.97	          2.833866
Octuple     	97.92	          3.701376

Harvest Levels: STONE, STONE, STONE, IRON, IRON, IRON,
		DIAMOND, DIAMOND

**Compressed Iron**

![](http://i.imgur.com/BYtyvCd.png)

Spoiler (click to hide)

TIER    	HARDNESS	RESISTANCE
Compressed	  5.138 	  1.202292
Double  	 20.552 	  4.809168
Triple  	 46.242 	 10.820628
Quadruple  	 82.208 	 19.236672
Quintuple	128.45  	 30.0573
Sextuple	184.968 	 43.282512
Septuple	251.762 	 58.912308
Octuple 	328.832 	 76.946688

Harvest Levels: IRON, IRON, IRON, IRON, IRON, DIAMOND,
		DIAMOND, DIAMOND

**Compressed Endstone**

![](http://i.imgur.com/QqvhpOx.png)

Spoiler (click to hide)

TIER    	HARDNESS	RESISTANCE
Compressed	  3     	  0.525
Double  	 12     	  2.1
Triple  	 27     	  4.725
Quadruple	 48     	  8.4
Quintuple	 75     	 13.125
Sextuple	108     	 18.9
Septuple	147     	 25.725
Octuple 	192     	 33.6

Harvest Levels: IRON, IRON, IRON, IRON, IRON, DIAMOND,
		DIAMOND, DIAMOND

**Compressed Ender Pearl**

![](http://i.imgur.com/ZY3pMw3.png)

Spoiler (click to hide)

TIER    	HARDNESS	RESISTANCE
Compressed	  1.5    	  0.2625
Double  	  6     	  1.05
Triple  	 13.5    	  2.3625
Quadruple	 24     	  4.2
Quintuple	 37.5    	  6.5625
Sextuple	 54     	  9.45
Septuple	 73.5   	 12.8625
Octuple 	 96     	 16.8

Harvest Levels: STONE, STONE, STONE, IRON, IRON, IRON,
		DIAMOND, DIAMOND

**Carbon**

![](http://i1.wp.com/kreezcraft.com/wp-content/uploads/2015/11/activated-carbonite.png?fit=810,810)

This is a chain of carbon related material. It will be the basis of more advanced technology. Currently it provides an alternate pathway to diamonds from both coal and charcoal. You might recognize some of the names I used, that will be the extent of any similarity. It is fantasy after all. Still if you have a suggestion for tech based on it, fire away!

Spoiler (click to hide)

**Coal and Gravel Mix**

![](http://i.imgur.com/iWS4wzI.png)

Yields 2 blocks when a block of coal and block of gravel are combined. Drops 3 carbon dust if mined with a pickaxe.

**Activated Carbonite**

![](http://i.imgur.com/O9e4jwN.png)

Drops only itself when harvest. However, if placed in a furnace for baking, it will yield 18 carbon dust. Also emanates particles when placed to let you know it has a higher purpose in life.

**Coal Block made from Charcoal**

![](http://i.imgur.com/X6TgeDs.png)

This is probably the first 1.8 recipe for turn charcoal into coal. This recipe quite possibly will make the carbon chain depicted here the preferable path to diamonds. Considering that you can easily create advanced automated factories now in 1.8.

**Unfired Bucky Ball**

![](http://i.imgur.com/57togbt.png)

I'm sure someone from MIT will point out this is not really a bucky ball. The name is really tongue in cheek as there is quite a bit of carbon invested in it by this stage.

**Bucky Ball**

![](http://i.imgur.com/iBiLDgo.png)

Now that you have baked it, you can use it. It is one of the basic components of my carbon tech tree. Just like in real life.

**Unfired Carbon Nanotube**

![](http://i.imgur.com/UQsvviA.png)

It's made from buckyballs also like in real life. I'm not really sure how they are crafted in real life tho, so this will have to do.

**Carbon Nanotube**

![](http://i.imgur.com/MT38nFg.png)

Again with the baking!? Yeah, so what, just think of it as if you are impregnating it with carbon filaments provided by the furnace component. Also, this is a basic element in the design of other items in the carbon tech tree.

**Unfired Carbon Mesh**

![](http://i.imgur.com/RE8jVKg.png)

Ingredients: 4 buckyballs, 1 unfired buckyball in the center, and 4 carbon nanotubes.

**Carbon Mesh**

![](http://i.imgur.com/hFwypHS.png)

Even more baking! Now we've completed all of the basic components for are carbon technology. Hint: don't bake everything, leave some of it unfired as both types in each category are used for the tech recipes.

**Unfired Diamond**

![](http://i.imgur.com/7LHcjqN.png)

Ingredients: 1 unfired carbon mesh, 4 carbon mesh, 2 carbon nanotubes, and 2 buckyballs.

**Diamond**

![](http://i.imgur.com/mT1y82L.png)

Yet more baking! But now you have 32precious diamonds and 256 xp for doing it! Maybe you should put them in a glass case for all to see! This path is far more expensivethan the compressed coal block route which is why I'm giving you a ton of xp for taking it. Make a tree farm, convert logs to charcoal and then a chain of crafting and smelting machines via a pipeline or conveyor and you will have the potential to have insane xp which I don't have to tell you is perfect for enchanting. Let me know if this too op and I'll make the chain harder.

Also, configs are coming in the future so you can tweak stuff like this for your lan parties and servers.

## Modpacks

Yes you can use my mod in your pack.

## Help A Veteran Today

I am US Veteran. I am not disabled. However, I spend a lot of time building this mods and modpacks for you. Please help me to help you by donating at [https://patreon.com/kreezxil](https://patreon.com/kreezxil) .

**This project is proudly powered by FORGE**, without whom it would not be possible. Help FORGE get rid of Adfocus at [https://www.patreon.com/LexManos](https://www.patreon.com/LexManos).
